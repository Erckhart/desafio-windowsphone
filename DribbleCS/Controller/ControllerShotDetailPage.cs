﻿using Dribbble.Controller.Net.Conn;
using Dribbble.Controller.Util;
using Dribbble.Model;
using Dribbble.View;
using Microsoft.Phone.Shell;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Navigation;

namespace Dribbble.Controller
{
    public class ControllerShotDetailPage : BindableBase
    {

        private ShotBO shot;

        public ShotBO Shot
        {
            get { return shot; }
            set
            {
                SetProperty<ShotBO>(ref shot, value, "Shot");
            }
        }

        private ObservableCollection<CommentBO> comments;

        public ObservableCollection<CommentBO> Comments
        {
            get { return comments; }
            set
            {
                SetProperty<ObservableCollection<CommentBO>>(ref comments, value, "Comments");
            }
        }
        
        private DribbbleHelper mDribbbleHelper;

        public ControllerShotDetailPage()
        {
            mDribbbleHelper = new DribbbleHelper();
            Shot = new ShotBO();
        }

        public void LoadShot()
        {
            Shot = PhoneApplicationService.Current.State[ShotDetailPage.SHOT_KEY] as ShotBO;
        }

        public async Task LoadCommentsAsync()
        {
            Comments = new ObservableCollection<CommentBO>(await mDribbbleHelper.GetCommentsShotAsync(Shot.ID));
        }

        public void NavigateToPlayerShots(NavigationService navService)
        {
            Dictionary<string, object> bundle = new Dictionary<string, object>();
            bundle.Add(PlayerShotsPage.PLAYER_KEY, Shot.User);
            NavigationHelper.NavigateTo(navService, typeof(PlayerShotsPage), bundle);
        }
        
    }
}
