﻿using DribbleCS.Model;
using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Windows.Data;

namespace Dribbble.Controller.Converter
{
    public class ShotFilterConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        public static ObservableCollection<EShotFilter> GetFilterTypes()
        {
            var types = Enum.GetValues(typeof(EShotFilter));
            var collection = new ObservableCollection<EShotFilter>();
            foreach (var item in types)
            {
                collection.Add((EShotFilter)item);
            }
            return collection;
        }


    }
}
