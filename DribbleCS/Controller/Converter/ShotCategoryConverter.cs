﻿using DribbleCS.Model;
using DribbleCS.Resources;
using System;
using System.Globalization;
using System.Windows.Data;

namespace Dribbble.Controller.Converter
{
    public class ShotCategoryConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return ConvertToString((EShotCategory)value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return ConvertToEnum(value.ToString());
        }

        private EShotCategory ConvertToEnum(string v)
        {
            if (AppResources.CategoryComments.Contains(v))
            {
                return EShotCategory.COMMENTS;
            }
            else if (AppResources.CategoryPopular.Contains(v))
            {
                return EShotCategory.POPULAR;
            }
            else if (AppResources.CategoryRecents.Contains(v))
            {
                return EShotCategory.RECENTS;
            }
            else
            {
                return EShotCategory.VIEWS;
            }
        }

        private string ConvertToString(EShotCategory category)
        {
            switch (category)
            {
                case EShotCategory.COMMENTS:
                    return AppResources.CategoryComments;
                case EShotCategory.POPULAR:
                    return AppResources.CategoryPopular;
                case EShotCategory.RECENTS:
                    return AppResources.CategoryRecents;
                case EShotCategory.VIEWS:
                    return AppResources.CategoryViews;
                default:
                    return AppResources.CategoryPopular;
            }
        }

    }
}
