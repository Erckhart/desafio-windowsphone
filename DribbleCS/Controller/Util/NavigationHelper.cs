﻿using Microsoft.Phone.Shell;
using System;
using System.Collections.Generic;
using System.Windows.Navigation;

namespace Dribbble.Controller.Util
{
    public class NavigationHelper
    {

        private const string BASE = "/View/{0}.xaml";

        public static void NavigateTo(NavigationService navService, Type to)
        {
            NavigateTo(navService, to, null);
        }

        public static void NavigateTo(NavigationService navService, Type to, Dictionary<string, Object> bundle)
        {

            if (bundle != null)
            {
                foreach (string key in bundle.Keys)
                {
                    PhoneApplicationService.Current.State[key] = bundle[key];
                }
            }
            string url = string.Format(BASE, to.Name);
            navService.Navigate(new Uri(url, UriKind.Relative));
        }

    }
}
