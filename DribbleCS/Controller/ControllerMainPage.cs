﻿using Dribbble.Controller.Converter;
using Dribbble.Controller.Net.Conn;
using Dribbble.Controller.Util;
using Dribbble.Model;
using Dribbble.View;
using Dribbble.View.Control;
using DribbleCS.Model;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Navigation;

namespace Dribbble.Controller
{
    public class ControllerMainPage
    {

        public ObservableCollection<ShotCategoryBO> Categories { get; private set; }

        public StatusBO Status { get; private set; }
        
        private DribbbleHelper mDribbbleHelper;
        
        public ControllerMainPage()
        {
            Status = new StatusBO();
            Status.IsLoading = true;
            mDribbbleHelper = new DribbbleHelper();
            
            Categories = ShotCategoryBO.GetCategories(obj => OnScrollReachesBottom(obj));

        }

        private async void OnScrollReachesBottom(ShotCategoryBO obj)
        {
            if (!obj.IsLoading)
            {
                await LoadMoreShots(obj);
            }
        }
        
        public async Task LoadShots(ShotCategoryBO category)
        {
            category.IsLoading = true;
            Status.IsLoading = true;
            Status.Message = "Loading Shots";
            var shoots = await mDribbbleHelper.GetShotByCategoryAsync(category.Category, category.PageNumber, category.Filter);
            foreach(var shot in shoots)
            {
                category.Shots.Add(shot);
            }
            Status.IsLoading = false;
            category.IsLoading = false;
            Status.Message = "Shots Loaded";
        }

        public async Task ReloadShots(ShotCategoryBO category)
        {
            category.Reset();
            await LoadShots(category);
        }

        public async Task LoadMoreShots(ShotCategoryBO category)
        {
            category.PageNumber++;
            await LoadShots(category);
        }
        
        public void NavigateTo(NavigationService navService, ShotBO shot)
        {
            Dictionary<string,object> bundle = new Dictionary<string, object>();
            bundle.Add(ShotDetailPage.SHOT_KEY, shot);
            NavigationHelper.NavigateTo(navService, typeof(ShotDetailPage), bundle);
        }

        public void NavigateToSettings(NavigationService navService)
        {
            NavigationHelper.NavigateTo(navService, typeof(SettingsPage));
        }

    }
}
