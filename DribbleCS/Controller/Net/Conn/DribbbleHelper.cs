﻿using Dribbble.Model;
using DribbleCS.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Windows.Web.Http;

namespace Dribbble.Controller.Net.Conn
{
    public class DribbbleHelper
    {

        private const string ACCESS_TOKEN = "access_token=6b65e62f78092b873915ca235b7230e9c2d742849c3b1ded0645c2be7371a7cb";

        private const string COMMENTS_SHOTS_URL = "http://api.dribbble.com/shots/{0}/comments";
        private const string PLAYER_SHOTS_URL = "http://api.dribbble.com/v1/users/{0}/shots?{1}";
        private const string SHOTS_CATEGORY_URL = "http://api.dribbble.com/v1/shots?sort={0}&page={1}&list={2}&{3}";

        public async Task<List<ShotBO>> GetShotByCategoryAsync(EShotCategory category, int pageNumber, EShotFilter filter)
        {
            string uri = string.Format(SHOTS_CATEGORY_URL, category.ToString().ToLower(), pageNumber,filter.ToString().ToLower(), ACCESS_TOKEN);
            string json = string.Empty;
            try
            {
                using (var client = new HttpClient()) { 
                    json = await client.GetStringAsync(new Uri(uri,UriKind.Absolute));
                }
            }
            catch (Exception e)
            {
                var msg = e.Message;
            }
            var shots = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ShotBO>>(json);
            return shots;
        }

        public async Task<List<CommentBO>> GetCommentsShotAsync(int shotID)
        {
            string uri = string.Format(COMMENTS_SHOTS_URL, shotID);
            string json = string.Empty;
            using (var client = new HttpClient())
            {
                json = await client.GetStringAsync(new Uri(uri));
            }
            var page = Newtonsoft.Json.JsonConvert.DeserializeObject<CommentsPageBO>(json);
            return page.Comments;
        }

        public async Task<List<ShotBO>> GetPlayerShotsAsync(int playerID)
        {
            string uri = string.Format(PLAYER_SHOTS_URL, playerID, ACCESS_TOKEN);
            string json = string.Empty;
            using (var client = new HttpClient())
            {
                json = await client.GetStringAsync(new Uri(uri));
            }
            var comments = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ShotBO>>(json);
            return comments;
        }

    }
}
