﻿using Dribbble.Controller.Net.Conn;
using Dribbble.Controller.Util;
using Dribbble.Model;
using Dribbble.View;
using Microsoft.Phone.Shell;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Navigation;

namespace Dribbble.Controller
{
    public class ControllerPlayerShotPage : BindableBase
    {
        private PlayerBO player;

        public PlayerBO Player
        {
            get { return player; }
            set
            {
                SetProperty<PlayerBO>(ref player, value, "Player");
            }
        }

        private ObservableCollection<ShotBO> shots;

        public ObservableCollection<ShotBO> Shots
        {
            get { return shots; }
            set
            {
                SetProperty<ObservableCollection<ShotBO>>(ref shots, value, "Shots");
            }
        }


        private DribbbleHelper mDribbbleHelper;

        public ControllerPlayerShotPage()
        {
            mDribbbleHelper = new DribbbleHelper();
        }

        public void LoadPlayer()
        {
            Player = PhoneApplicationService.Current.State[PlayerShotsPage.PLAYER_KEY] as PlayerBO;
        }

        public async Task LoadShots()
        {
            Shots = new ObservableCollection<ShotBO>(await mDribbbleHelper.GetPlayerShotsAsync(Player.ID));
        }

        internal void NavigateToShotDetail(NavigationService naviagtionService, ShotBO item)
        {
            item.User = Player;
            Dictionary<string, object> bundle = new Dictionary<string, object>();
            bundle.Add(ShotDetailPage.SHOT_KEY, item);
            NavigationHelper.NavigateTo(naviagtionService, typeof(ShotDetailPage), bundle);
        }
    }
}
