﻿using Dribbble.Controller;
using Dribbble.Model;
using Microsoft.Phone.Controls;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Navigation;

namespace Dribbble.View
{
    public partial class PlayerShotsPage : PhoneApplicationPage
    {
        public const string PLAYER_KEY = "PK";

        private ControllerPlayerShotPage mController;

        public PlayerShotsPage()
        {
            InitializeComponent();
            mController = new ControllerPlayerShotPage();
            DataContext = mController;
        }

        protected async override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            mController.LoadPlayer();
            await mController.LoadShots();
        }

        private void OnShotSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var item = e.AddedItems.Cast<ShotBO>().FirstOrDefault();
            if (item != null)
            {
                mController.NavigateToShotDetail(NavigationService, item);
            }
        }
    }
}