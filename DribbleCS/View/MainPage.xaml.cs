﻿using Dribbble.Controller;
using Dribbble.Model;
using Dribbble.View.Control;
using DribbleCS.Model;
using Microsoft.Phone.Controls;
using System.Linq;
using System.Windows;
using System.Windows.Navigation;

namespace Dribbble.View
{
    public partial class MainPage : PhoneApplicationPage
    {

        private ControllerMainPage mControllerMainPage;
        
        public MainPage()
        {
            InitializeComponent();
            mControllerMainPage = new ControllerMainPage();
            DataContext = mControllerMainPage;
        }
        
        protected async override void OnNavigatedTo(NavigationEventArgs e)
        {
            foreach (var item in mControllerMainPage.Categories)
            {
                await mControllerMainPage.LoadShots(item);
            }
        }

        private void OnShotItemSelected(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            var item = e.AddedItems.Cast<ShotBO>().First();
            if (item != null)
            {
                mControllerMainPage.NavigateTo(NavigationService, item);
            }
        }

        private void OnSettingsMenuClicked(object sender, System.EventArgs e)
        {
            mControllerMainPage.NavigateToSettings(NavigationService);
        }
        
        private async void OnRefreshRequested(object sender, System.EventArgs e)
        {
            var category = ((FrameworkElement)sender).DataContext as ShotCategoryBO;
            if (category != null)
            {
                await mControllerMainPage.ReloadShots(category);
            }
        }

        private async void OnFilterChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            var picker = (ShotFilterPicker)sender;
            var category = picker.DataContext as ShotCategoryBO;
            if (category != null)
            {
                category.Filter = (EShotFilter) picker.SelectedItem;
                await mControllerMainPage.ReloadShots(category);
            }
        }
    }
}