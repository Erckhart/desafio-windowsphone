﻿using Microsoft.Phone.Controls;

namespace Dribbble.View.Control
{
    public class ShotFilterPicker : ListPicker
    {

        public ShotFilterPicker()
        {
            SetValue(ListPicker.ItemCountThresholdProperty, 7);
        }

    }
}
