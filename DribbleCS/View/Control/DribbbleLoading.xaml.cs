﻿using System.Windows;
using System.Windows.Controls;

namespace Dribbble.View.Control
{
    public partial class DribbbleLoading : UserControl
    {
        public DribbbleLoading()
        {
            InitializeComponent();
        }

        private void LayoutRoot_Loaded(object sender, RoutedEventArgs e)
        {
            this.SpinningAnimation.Begin();
        }
    }
}
