﻿using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Dribbble.Controller;
using Dribbble.Controller.Util;

namespace Dribbble.View
{
    public partial class ShotDetailPage : PhoneApplicationPage
    {

        public const string SHOT_KEY = "SHOT_BO";

        private ControllerShotDetailPage mControllerShotDetailPage;

        public ShotDetailPage()
        {
            InitializeComponent();
            mControllerShotDetailPage = new ControllerShotDetailPage();
            DataContext = mControllerShotDetailPage;
        }

        protected async override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            mControllerShotDetailPage.LoadShot();
            await mControllerShotDetailPage.LoadCommentsAsync();
        }

        private void OnShotCountClick(object sender, System.Windows.Input.GestureEventArgs e)
        {
            mControllerShotDetailPage.NavigateToPlayerShots(NavigationService);
        }

        private void OnShareMenuClick(object sender, System.EventArgs e)
        {

        }
        
    }
}