﻿using Dribbble.Model;
using System.Collections.Generic;

namespace DribbleCS.Model
{
    public class CommentsPageBO
    {

        public int Page { get; set; }

        public List<CommentBO> Comments { get; set; }

    }
}
