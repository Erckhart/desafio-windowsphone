﻿namespace Dribbble.Model
{
    public class CommentBO
    {

        public int ID { get; set; }
        public string Body { get; set; }
        public PlayerBO Player { get; set; }

        [Newtonsoft.Json.JsonProperty("likes_count")]
        public int Likes { get; set; }

    }
}
