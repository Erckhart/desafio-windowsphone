﻿using DribbleCS.Model;

namespace Dribbble.Model
{
    public class ShotBO
    {

        public int ID { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string URL { get; set; }
        public PlayerBO User { get; set; }

        [Newtonsoft.Json.JsonProperty("comments_count")]
        public int Comments { get; set; }

        [Newtonsoft.Json.JsonProperty("likes_count")]
        public int Likes { get; set; }

        [Newtonsoft.Json.JsonProperty("images")]
        public ShotImageBO ImageUri { get; set; }

        [Newtonsoft.Json.JsonProperty("views_count")]
        public int Views { get; set; }
    }
}
