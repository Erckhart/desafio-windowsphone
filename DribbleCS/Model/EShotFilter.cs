﻿namespace DribbleCS.Model
{
    public enum EShotFilter
    {

        ANY,
        ANIMATED,
        ATTACHMENTS,
        DEBUTS,
        PLAYOFFS,
        REBOUNDS,
        TEAMS

    }
}
