﻿namespace Dribbble.Model
{
    public class StatusBO : BindableBase
    {

        private string message;

        public string Message
        {
            get { return message; }
            set
            {
                SetProperty<string>(ref message, value, "Message");
            }
        }

        private bool isLoading;

        public bool IsLoading
        {
            get { return isLoading; }
            set
            {
                SetProperty<bool>(ref isLoading, value, "IsLoading");
            }
        }



    }
}
