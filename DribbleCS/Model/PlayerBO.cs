﻿namespace Dribbble.Model
{
    public class PlayerBO
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Bio { get; set; }

        [Newtonsoft.Json.JsonProperty("html_url")]
        public string URL { get; set; }

        [Newtonsoft.Json.JsonProperty("likes_count")]
        public int Likes { get; set; }

        [Newtonsoft.Json.JsonProperty("likes_received_count")]
        public int LikesReceived { get; set; }

        [Newtonsoft.Json.JsonProperty("views_count")]
        public int Views { get; set; }

        [Newtonsoft.Json.JsonProperty("comments_count")]
        public int Comments { get; set; }

        [Newtonsoft.Json.JsonProperty("shots_count")]
        public int Shots { get; set; }

        [Newtonsoft.Json.JsonProperty("avatar_url")]
        public string Avatar { get; set; }

    }
}
