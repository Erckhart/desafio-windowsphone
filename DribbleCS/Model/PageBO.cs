﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dribbble.Model
{
    public class PageBO
    {

        public int ID { get; private set; }
        public List<ShotBO> Shots {get;set;}

    }
}
