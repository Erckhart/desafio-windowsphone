﻿using Dribbble.Controller.Converter;
using Dribbble.Model;
using Dribbble.View.Control;
using System;
using System.Linq;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace DribbleCS.Model
{
    public class ShotCategoryBO : BindableBase
    {

        public ShotCategoryBO()
        {
            Filter = EShotFilter.ANY;
            IsLoading = true;
            Filters = ShotFilterConverter.GetFilterTypes();
            Shots = new ObservableCollection<ShotBO>();
            OnScrollReachesBottomCommand = new DelegateCommand( obj => NotifyScrollReachedBottom(this));
        }

        private void NotifyScrollReachedBottom(ShotCategoryBO category)
        {
            if (OnScrollReachesBottomListener != null)
            {
                OnScrollReachesBottomListener(category);
            }
        }

        private bool isLoading;

        public bool IsLoading
        {
            get { return isLoading; }
            set
            {
                SetProperty<bool>(ref isLoading, value, "IsLoading");
            }
        }

        private int pageNumber;

        public int PageNumber
        {
            get { return pageNumber; }
            set { pageNumber = value; }
        }

        private EShotFilter filter;

        public EShotFilter Filter
        {
            get { return filter; }
            set
            {
                SetProperty<EShotFilter>(ref filter, value, "Filter");
            }
        }

        private ObservableCollection<ShotBO> shots;

        public ObservableCollection<ShotBO> Shots
        {
            get { return shots; }
            set
            {
                SetProperty<ObservableCollection<ShotBO>>(ref shots, value, "Shots");
            }
        }
        
        private ObservableCollection<EShotFilter> filters;

        public ObservableCollection<EShotFilter> Filters
        {
            get { return filters; }
            set
            {
                SetProperty<ObservableCollection<EShotFilter>>(ref filters, value, "Filters");
            }
        }

        private EShotCategory category;

        public EShotCategory Category
        {
            get { return category; }
            set
            {
                SetProperty<EShotCategory>(ref category, value, "Category");
            }
        }

        public void Reset()
        {
            PageNumber = 1;
            Shots.Clear();
        }
        
        public static ObservableCollection<ShotCategoryBO> GetCategories(Action<ShotCategoryBO> action)
        {
            var categories = new ObservableCollection<ShotCategoryBO>();
            var eCategories = Enum.GetValues(typeof(EShotCategory));
            foreach (var item in eCategories)
            {
                var category = new ShotCategoryBO()
                {
                    Category = (EShotCategory)item,
                    PageNumber = 1
                };
                category.OnScrollReachesBottomListener += action;
                categories.Add(category);
            }
            return categories;
        }

        public ICommand OnScrollReachesBottomCommand { get; private set; }

        public Action<ShotCategoryBO> OnScrollReachesBottomListener { get; set; }

        public override string ToString()
        {
            return Category.ToString();
        }

    }
}
